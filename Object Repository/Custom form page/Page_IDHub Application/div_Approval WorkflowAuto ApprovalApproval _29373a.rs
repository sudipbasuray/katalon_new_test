<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Approval WorkflowAuto ApprovalApproval _29373a</name>
   <tag></tag>
   <elementGuidId>c2152d75-0284-47e4-80d5-c86f4ba38045</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card.right-card</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='add-app-details-form']/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card right-card</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Approval WorkflowAuto ApprovalApproval Workflow*Access ManagerFulfiller*CertifiableAttach a Request Form You can now attach a customized form for your application that will be asked to requester to fill for every access request for the application. The list includes the custom forms created by you in &quot;Custom Forms&quot; section. To create a new form for your application, click here.&quot;click here&quot; will redirect user to 'Custom Forms section in a new tab  Test Form 1 Select Custom FormRisk Level*1 - low2 - medium3 - high  Requestable </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;add-app-details-form&quot;)/div[@class=&quot;card right-card&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='add-app-details-form']/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Integration Level*'])[1]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
