<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Application Name</name>
   <tag></tag>
   <elementGuidId>b2e09e87-3817-4f35-9686-64a65ea52325</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.mat-form-field-infix.ng-tns-c99-60</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='add-app-details-form']/div/div/div[2]/div[2]/mat-form-field/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mat-form-field-infix ng-tns-c99-60</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Application Name*</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;add-app-details-form&quot;)/div[@class=&quot;card left-card&quot;]/div[@class=&quot;basic-details-block&quot;]/div[@class=&quot;idh-row&quot;]/div[@class=&quot;idh-col-6 relative&quot;]/mat-form-field[@class=&quot;mat-form-field ng-tns-c99-60 mat-primary mat-form-field-type-mat-input mat-form-field-appearance-fill mat-form-field-can-float mat-form-field-has-label ng-untouched ng-pristine ng-invalid ng-star-inserted mat-form-field-should-float mat-focused&quot;]/div[@class=&quot;mat-form-field-wrapper ng-tns-c99-60&quot;]/div[@class=&quot;mat-form-field-flex ng-tns-c99-60&quot;]/div[@class=&quot;mat-form-field-infix ng-tns-c99-60&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='add-app-details-form']/div/div/div[2]/div[2]/mat-form-field/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Application'])[1]/following::div[19]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='chevron_left'])[1]/following::div[19]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search Keywords'])[1]/preceding::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//mat-form-field/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
