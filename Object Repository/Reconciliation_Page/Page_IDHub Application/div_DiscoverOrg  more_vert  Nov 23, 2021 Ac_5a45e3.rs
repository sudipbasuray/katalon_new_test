<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_DiscoverOrg  more_vert  Nov 23, 2021 Ac_5a45e3</name>
   <tag></tag>
   <elementGuidId>beb0b343-713f-4143-bbc4-efd3f13a823b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.d-flex.access-body</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='profile-my-access']/div[2]/div[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>d-flex access-body</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> DiscoverOrg  more_vert  Nov 23, 2021 AccessProvisionedAccount ID jerome </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;profile-my-access&quot;)/div[@class=&quot;left-profile-card access-container&quot;]/div[@class=&quot;main-content-container ng-star-inserted&quot;]/div[@class=&quot;d-flex access-body&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='profile-my-access']/div[2]/div[2]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Date added'])[1]/following::div[9]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//app-my-access/div/div[2]/div[2]/div</value>
   </webElementXpaths>
</WebElementEntity>
