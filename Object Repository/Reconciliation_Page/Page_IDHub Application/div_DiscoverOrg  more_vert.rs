<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_DiscoverOrg  more_vert</name>
   <tag></tag>
   <elementGuidId>372fb61d-d654-4484-82f3-54cd8e77f2f7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.descriptive-content-bold.d-flex</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='profile-my-access']/div[2]/div[2]/div/div[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>descriptive-content-bold d-flex</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> DiscoverOrg  more_vert </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;profile-my-access&quot;)/div[@class=&quot;left-profile-card access-container&quot;]/div[@class=&quot;main-content-container ng-star-inserted&quot;]/div[@class=&quot;d-flex access-body&quot;]/div[@class=&quot;main-content&quot;]/div[@class=&quot;descriptive-content-bold d-flex&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='profile-my-access']/div[2]/div[2]/div/div[2]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Date added'])[1]/following::div[12]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//app-my-access/div/div[2]/div[2]/div/div[2]/div</value>
   </webElementXpaths>
</WebElementEntity>
