<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_No direct reports found_mat-drawer-back_72676f</name>
   <tag></tag>
   <elementGuidId>0dfe768d-d34f-4c44-b5e1-e0106f70fead</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.mat-drawer-backdrop.ng-star-inserted.mat-drawer-shown</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//mat-sidenav-container/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mat-drawer-backdrop ng-star-inserted mat-drawer-shown</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;hideOverflow&quot;]/app-root[1]/app-core-layout[1]/div[@class=&quot;main-wrapper d-flex&quot;]/section[@class=&quot;page-container&quot;]/app-profile[@class=&quot;ng-star-inserted&quot;]/app-side-panel[1]/mat-sidenav-container[@class=&quot;mat-drawer-container mat-sidenav-container example-container mat-drawer-transition&quot;]/div[@class=&quot;mat-drawer-backdrop ng-star-inserted mat-drawer-shown&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//mat-sidenav-container/div</value>
   </webElementXpaths>
</WebElementEntity>
