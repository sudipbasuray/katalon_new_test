<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_LicenseIf you have purchased a plan the_e8cbb8</name>
   <tag></tag>
   <elementGuidId>dd6de957-91b9-4de6-a048-404b23e0ea0e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[2]/div[2]/div[2]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.top-container</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>top-container</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>LicenseIf you have purchased a plan then enter the license key. You can start 30 days trial or purchase. Enter license keyywq4iw6y-adg3-6gad-adh3-hd2ak34khjkhFetching license key details...Generate trial keyGet 30 days trial license nowDon’t have a key? Buy now</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;mat-typography&quot;]/app-root[1]/app-tenant-registration-wizard[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;setup-container&quot;]/div[@class=&quot;body-content&quot;]/div[@class=&quot;body&quot;]/div[@class=&quot;license ng-star-inserted&quot;]/div[@class=&quot;top-container&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div[2]/div</value>
   </webElementXpaths>
</WebElementEntity>
