<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_close_user-icon-wrapper user-hover</name>
   <tag></tag>
   <elementGuidId>82c7276b-c577-445b-86b7-c921fd54b9bc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//mat-expansion-panel-header[@id='mat-expansion-panel-header-6']/span/mat-panel-title/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>mat-panel-title.mat-expansion-panel-header-title.ng-tns-c152-104 > div.d-flex.expansion-panel-title.max-width > div.idh-col.no-padding-lr.certification-user-icon.user-list-icon-wrapper > div.user-icon-wrapper.user-hover</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>user-icon-wrapper user-hover</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mat-expansion-panel-header-6&quot;)/span[@class=&quot;mat-content ng-tns-c152-104&quot;]/mat-panel-title[@class=&quot;mat-expansion-panel-header-title ng-tns-c152-104&quot;]/div[@class=&quot;d-flex expansion-panel-title max-width&quot;]/div[@class=&quot;idh-col no-padding-lr certification-user-icon user-list-icon-wrapper&quot;]/div[@class=&quot;user-icon-wrapper user-hover&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//mat-expansion-panel-header[@id='mat-expansion-panel-header-6']/span/mat-panel-title/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='close'])[2]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Enter reason for'])[1]/following::div[7]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Certify'])[2]/preceding::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='more_vert'])[2]/preceding::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//mat-expansion-panel[2]/mat-expansion-panel-header/span/mat-panel-title/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
