<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Select all_user-icon-wrapper user-selected</name>
   <tag></tag>
   <elementGuidId>37259d75-a844-4f55-91d5-16768a96e37f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.user-icon-wrapper.user-selected</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Certify'])[1]/preceding::div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>user-icon-wrapper user-selected</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mat-expansion-panel-header-8&quot;)/span[@class=&quot;mat-content ng-tns-c152-121&quot;]/mat-panel-title[@class=&quot;mat-expansion-panel-header-title ng-tns-c152-121&quot;]/div[@class=&quot;d-flex expansion-panel-title max-width user-list-cursor&quot;]/div[@class=&quot;idh-col no-padding-lr certification-user-icon user-list-icon-wrapper&quot;]/div[@class=&quot;user-icon-wrapper user-selected&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//mat-expansion-panel-header[@id='mat-expansion-panel-header-8']/span/mat-panel-title/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Catalog items need to be certified (6)'])[1]/following::div[10]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Certify'])[1]/preceding::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='more_vert'])[1]/preceding::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//mat-panel-title/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
