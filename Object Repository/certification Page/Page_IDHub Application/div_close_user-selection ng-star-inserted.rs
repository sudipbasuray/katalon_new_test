<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_close_user-selection ng-star-inserted</name>
   <tag></tag>
   <elementGuidId>b2f3be45-8173-49e7-a0c4-9d6755cdd916</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//mat-expansion-panel-header[@id='mat-expansion-panel-header-7']/span/mat-panel-title/div/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>mat-panel-title.mat-expansion-panel-header-title.ng-tns-c152-106 > div.d-flex.expansion-panel-title.max-width.user-list-cursor > div.idh-col.no-padding-lr.certification-user-icon.user-list-icon-wrapper > div.user-icon-wrapper > div.user-selection.ng-star-inserted</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>user-selection ng-star-inserted</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mat-expansion-panel-header-7&quot;)/span[@class=&quot;mat-content ng-tns-c152-106&quot;]/mat-panel-title[@class=&quot;mat-expansion-panel-header-title ng-tns-c152-106&quot;]/div[@class=&quot;d-flex expansion-panel-title max-width user-list-cursor&quot;]/div[@class=&quot;idh-col no-padding-lr certification-user-icon user-list-icon-wrapper&quot;]/div[@class=&quot;user-icon-wrapper&quot;]/div[@class=&quot;user-selection ng-star-inserted&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//mat-expansion-panel-header[@id='mat-expansion-panel-header-7']/span/mat-panel-title/div/div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//mat-expansion-panel[3]/mat-expansion-panel-header/span/mat-panel-title/div/div/div/div</value>
   </webElementXpaths>
</WebElementEntity>
