<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Submit (1)</name>
   <tag></tag>
   <elementGuidId>23673333-4bf7-45cf-b7bb-cfac67479a25</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Share your justification for this request *'])[1]/following::button[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.submit-button.primary-button.mat-button.mat-button-base.cdk-focused.cdk-mouse-focused</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>submit-button primary-button mat-button mat-button-base cdk-focused cdk-mouse-focused</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Submit </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-core-layout[1]/div[@class=&quot;main-wrapper d-flex&quot;]/section[@class=&quot;page-container page-container-without-menu&quot;]/app-my-request[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;request-wrapper&quot;]/div[@class=&quot;d-flex submit-request ng-star-inserted&quot;]/button[@class=&quot;submit-button primary-button mat-button mat-button-base cdk-focused cdk-mouse-focused&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Share your justification for this request *'])[1]/following::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jerome A.Smith'])[1]/following::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='close'])[2]/preceding::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/button</value>
   </webElementXpaths>
</WebElementEntity>
