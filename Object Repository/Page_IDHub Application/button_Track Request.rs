<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Track Request</name>
   <tag></tag>
   <elementGuidId>0fb49b86-18b2-4dc0-ae65-10b2f0db5cf4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Check Request Status'])[1]/following::button[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.button.secondary-button.mat-button.mat-button-base.cdk-focused.cdk-mouse-focused</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>button secondary-button mat-button mat-button-base cdk-focused cdk-mouse-focused</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Track Request</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-core-layout[1]/div[@class=&quot;main-wrapper d-flex&quot;]/section[@class=&quot;page-container page-container-without-menu&quot;]/app-layout[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;page-not-found-wrapper ng-star-inserted&quot;]/div[@class=&quot;page-not-found-container&quot;]/div[@class=&quot;page-not-found-content-wrapper&quot;]/div[@class=&quot;page-not-found-content&quot;]/app-dashboard[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;main-card-wrapper&quot;]/div[@class=&quot;idh-row card-wrapper ng-star-inserted&quot;]/div[@class=&quot;card-parent ng-star-inserted&quot;]/div[@class=&quot;card&quot;]/button[@class=&quot;button secondary-button mat-button mat-button-base cdk-focused cdk-mouse-focused&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Check Request Status'])[1]/following::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Follow Up On Your Requests'])[1]/following::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Check Your Tasks'])[1]/preceding::button[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/button</value>
   </webElementXpaths>
</WebElementEntity>
