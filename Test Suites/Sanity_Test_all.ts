<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Sanity_Test_all</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>78dedffb-667c-4ad5-ad64-900e30814668</testSuiteGuid>
   <testCaseLink>
      <guid>4d72f692-431e-44bb-80ae-04949c0f633b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/1.Tenant Management/Tenant Creation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b431242c-6c7b-457c-b03b-4bb2be73b9f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Basic/Basic Login to IDHub</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ee7a290e-273f-45bb-935f-fdb21616a2b9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>557efee6-7c98-4689-90aa-92f98dd4f3a4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2e44e744-cff3-4cfb-8544-95176c2651bd</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>358d9b4e-ed20-4699-be79-bc7cc72771b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/3.Application LifeCycle/add_Application_sanity</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f4cb77ce-9874-4c7d-aaac-686410596d4e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/4.Role Life Cycle/add_Role_sanity</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26582a8d-37b1-4c61-aa16-493ac2f2fe12</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/4.Role Life Cycle/Retire role and App</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0018edf7-15bb-47d8-a91c-f6136f3d4bf0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5.Certification/Certification_Life_Cycle</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
