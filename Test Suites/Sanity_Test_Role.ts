<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Sanity_Test_Role</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>7a61bcf8-a728-405b-9455-3143118ea5c2</testSuiteGuid>
   <testCaseLink>
      <guid>ad5c5336-ba45-43a2-b032-4c30acdfb571</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/4.Role Life Cycle/IUX-T8 (1.0) Request to Create Role by Wizard</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94acb846-6f1b-4cab-acdf-23483451b44e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/4.Role Life Cycle/Edit Role and retire Role</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
