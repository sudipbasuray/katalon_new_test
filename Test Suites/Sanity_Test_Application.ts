<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Sanity_Test_Application</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>dcd99ddc-c3ca-4e1c-8608-92addd885c78</testSuiteGuid>
   <testCaseLink>
      <guid>0554fc63-a0e7-40dc-ad80-d4a37dee19bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3.Application LifeCycle/IUX-T5 (1.0) Request to Create Application by Wizard</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f892906-c3e9-4a09-83ce-b2dfbd51190c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3.Application LifeCycle/Application Request and Retire</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
