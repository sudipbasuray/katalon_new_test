import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Basic/Basic Login to IDHub'), [('Username') : '', ('Password') : '', ('Organization') : ''], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/div_Certification'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Create Certification'))

WebUI.setText(findTestObject('Object Repository/certification Page/Page_IDHub Application/input_Create Certification_mat-input-2'), 
    'review of jerome')

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/div_User Access_mat-select-arrow-wrapper ng_a23313'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_User Access'))

WebUI.setText(findTestObject('Object Repository/certification Page/Page_IDHub Application/textarea_Search Keywords_mat-input-3'), 
    'Review of Jerome')

WebUI.setText(findTestObject('Object Repository/certification Page/Page_IDHub Application/input_None_mat-chip-list-input-1'), 
    'jerome')

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Jerome A.Smith'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/div_Select User_mat-select-arrow-wrapper ng_0f5930'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Certification'))

String emsg = WebUI.verifyTextPresent('Certification name is already exists', false, FailureHandling.CONTINUE_ON_FAILURE)

if (emsg.equals('true')) {
    int RN

    RN = ((Math.random() * 5000) as int)

    WebUI.setText(findTestObject('Object Repository/certification Page/Page_IDHub Application/input_Create Certification_mat-input-2'), 
        ('review access of jerome cert no-' + RN) + '')
}

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Next'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/i_add_circle_outline'))

WebUI.setText(findTestObject('Object Repository/certification Page/Page_IDHub Application/input_add_circle_outline_mat-chip-list-input-2'), 
    'Jerome')

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Jerome A.SmithOperation  UIC0042'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Done'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Next'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_By query'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/div_By query_mat-select-arrow-wrapper ng-tn_995642'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Disabled'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/div_Select or type_mat-select-arrow-wrapper_7c9756'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_equals ()'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/div_Select or type_mat-select-arrow-wrapper_b05693'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_false'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Next'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/div_Resource owner_mat-chip-list-wrapper'))

WebUI.setText(findTestObject('Object Repository/certification Page/Page_IDHub Application/input_Resource owner_mat-chip-list-input-3'), 
    'jerome')

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Jerome A.Smith'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/div_Select User_mat-select-arrow-wrapper ng_56a2ee'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Do not repeat'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Next'))

WebUI.setText(findTestObject('Object Repository/certification Page/Page_IDHub Application/input_Trigger Rule_mat-input-7'), 
    '1')

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Next'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Submit'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/div_Requests'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/div_Certification'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/i_more_vert'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/button_Run now'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Yes'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/div_Tasks'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Approve'))

WebUI.setText(findTestObject('Object Repository/certification Page/Page_IDHub Application/textarea_more_vert_mat-input-10'), 
    'Approve')

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Approve_1'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Manage Catalog'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/div_Tasks'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/div_Jerome A.Smith'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/i_more_vert'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/button_Revoke'))

WebUI.setText(findTestObject('Object Repository/certification Page/Page_IDHub Application/textarea_more_vert_mat-input-13'), 
    'revoked')

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Revoke'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Catalog items need to be certified (6)_0986bb'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/div_Select all_user-icon-wrapper user-selected'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Certify'))

WebUI.setText(findTestObject('Object Repository/certification Page/Page_IDHub Application/textarea_more_vert_mat-input-25'), 
    'certify')

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Certify_1'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Yes'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/div_Requests'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/div_Logout'))

WebUI.navigateToUrl(GlobalVariable.urlLogin)

WebUI.click(findTestObject('Object Repository/Page_IDHub/button_Login'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub/input_Login to your account_orgName'), GlobalVariable.tenantName)

WebUI.click(findTestObject('Object Repository/Page_IDHub/button_Login_1'))

WebUI.setText(findTestObject('Object Repository/certification Page/Page_Sign in to apple/input_Username or email_username'), 
    GlobalVariable.managerUsername)

WebUI.setText(findTestObject('Object Repository/certification Page/Page_Sign in to apple/input_Password_password'), GlobalVariable.password)

WebUI.click(findTestObject('Object Repository/certification Page/Page_Sign in to apple/input_Forgot Password_login'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Tasks'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Approve'))

WebUI.setText(findTestObject('Object Repository/certification Page/Page_IDHub Application/textarea_more_vert_mat-input-0'), 
    'Approve')

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Approve_1'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/div_Logout'))

WebUI.navigateToUrl(GlobalVariable.urlLogin)

WebUI.click(findTestObject('Object Repository/Page_IDHub/button_Login'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub/input_Login to your account_orgName'), GlobalVariable.tenantName)

WebUI.click(findTestObject('Object Repository/Page_IDHub/button_Login_1'))

WebUI.setText(findTestObject('Object Repository/certification Page/Page_Sign in to apple/input_Username or email_username'), 
    GlobalVariable.fulfillerUsername)

WebUI.setText(findTestObject('Object Repository/certification Page/Page_Sign in to apple/input_Password_password'), GlobalVariable.password)

WebUI.click(findTestObject('Object Repository/certification Page/Page_Sign in to apple/input_Forgot Password_login'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/div_Tasks1'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Claim'))

WebUI.setText(findTestObject('Object Repository/certification Page/Page_IDHub Application/textarea_more_vert_mat-input-1'), 
    'claim')

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Claim_1'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/i_more_vert'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/button_Complete'))

WebUI.setText(findTestObject('Object Repository/certification Page/Page_IDHub Application/textarea_more_vert_mat-input-2'), 
    'complete')

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Complete'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/div_Logout'))

WebUI.setText(findTestObject('Object Repository/certification Page/Page_Sign in to apple/input_Username or email_username'), 
    GlobalVariable.userName)

WebUI.setEncryptedText(findTestObject('Object Repository/certification Page/Page_Sign in to apple/input_Password_password'), 
    'tNEMIZtZeaU=')

WebUI.click(findTestObject('Object Repository/certification Page/Page_Sign in to apple/input_Forgot Password_login'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_My Profile'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/span_Revoked'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/img'))

WebUI.click(findTestObject('Object Repository/certification Page/Page_IDHub Application/div_Logout'))

WebUI.closeBrowser()

