import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Basic/Basic Login to IDHub'), [('Username') : '', ('Password') : '', ('Organization') : ''], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/div_Custom Forms'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/button_Create Form'))

WebUI.setText(findTestObject('Object Repository/Custom form page/Page_IDHub Application/input_Create Custom Form_mat-input-1'), 
    'Test Form 1')

WebUI.setText(findTestObject('Object Repository/Custom form page/Page_IDHub Application/textarea_Search Keywords_mat-input-2'), 
    'This is a Test form for automation testing')

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_Next'))

WebUI.setText(findTestObject('Object Repository/Custom form page/Page_IDHub Application/input_Create Custom Form_mat-input-4'), 
    'Basic Information')

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/p_Text Field'))

WebUI.setText(findTestObject('Object Repository/Custom form page/Page_IDHub Application/input_Label_mat-input-6'), 'UserName')

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/label_Required'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/p_Text Field'))

WebUI.setText(findTestObject('Object Repository/Custom form page/Page_IDHub Application/input_Label_mat-input-9'), 'Email')

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/label_Required'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_Submit'))

WebUI.setText(findTestObject('Object Repository/Custom form page/Page_IDHub Application/textarea_Submit_mat-input-11'), 
    'Submitting new form')

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_Submit_1'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/div_Manage Catalog'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/mat-icon_arrow_drop_down'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/button_Add Application'))

WebUI.setText(findTestObject('Object Repository/Custom form page/Page_IDHub Application/input_Add Application_mat-input-13'), 
    'Demo App 1')

WebUI.setText(findTestObject('Object Repository/Custom form page/Page_IDHub Application/textarea_Search Keywords_mat-input-14'), 
    'This is a demo app for Automation Testing')

WebUI.setText(findTestObject('Object Repository/Custom form page/Page_IDHub Application/input_Add To Collection_mat-chip-list-input-5'), 
    'Jerome')

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_Jerome A.Smith'))

WebUI.setText(findTestObject('Object Repository/Custom form page/Page_IDHub Application/input_Business Owner_mat-chip-list-input-6'), 
    'Jerome')

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_Jerome A.Smith'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/div_IDM_mat-select-arrow ng-tns-c132-65'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_Disconnected'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/div_Create users on Reconciliation_mat-sele_443619'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_Auto Approval'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/div_Approval Workflow_mat-select-arrow-wrap_cc8281'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_Access Manager'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/div_concat(, , click here, , )_mat-select-a_2432b1'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_Test Form 1'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/div_Select Custom Form_mat-slide-toggle-bar'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/label_Create users on Reconciliation'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_Next'))

WebUI.setText(findTestObject('Object Repository/Custom form page/Page_IDHub Application/input_Add Application_mat-input-16'), 
    'UserName')

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/div_Bi-Directional_mat-select-arrow-wrapper_229500'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_User Login'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/div_IDE Field Name_mat-select-arrow-wrapper_f9d4e4'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_String'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/div_Data Type_mat-slide-toggle-bar'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/div_Required_mat-slide-toggle-bar'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/div_Reconciliation key_mat-slide-toggle-bar'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_Add'))

WebUI.setText(findTestObject('Object Repository/Custom form page/Page_IDHub Application/input_Add Application_mat-input-16'), 
    'Email')

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/div_Bi-Directional_mat-select-arrow-wrapper_229500'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_Email'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/div_IDE Field Name_mat-select-arrow-wrapper_f9d4e4'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_String'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/div_Data Type_mat-slide-toggle-bar'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_Next'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_Submit'))

WebUI.setText(findTestObject('Object Repository/Custom form page/Page_IDHub Application/textarea_(0)_mat-input-20'), 'Submitting new applicaiton')

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_Submit'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/div_Tasks1'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_Claim'))

WebUI.setText(findTestObject('Object Repository/Custom form page/Page_IDHub Application/textarea_more_vert_mat-input-22'), 
    'claim')

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_Claim_1'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_Approve'))

WebUI.setText(findTestObject('Object Repository/Custom form page/Page_IDHub Application/textarea_more_vert_mat-input-23'), 
    'Approve')

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_Approve_1'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/div_Manage Catalog'))

WebUI.setText(findTestObject('Object Repository/Custom form page/Page_IDHub Application/input_Roles_searchKeyword'), 'Demo App 1')

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/span_Search'))

WebUI.click(findTestObject('Object Repository/Custom form page/Page_IDHub Application/div_Demo App 1    This is a demo app for Au_2e4443'))

WebUI.closeBrowser()

