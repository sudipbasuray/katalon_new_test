import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Basic/Basic Login to IDHub'), [('Username') : '', ('Password') : '', ('Organization') : ''], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Service_request/Page_IDHub Application/div_Service Request'))

WebUI.click(findTestObject('Object Repository/Service_request/Page_IDHub Application/button_Create Request'))

WebUI.setText(findTestObject('Object Repository/Service_request/Page_IDHub Application/input_Device Request_mat-input-1'), 
    'Keyboard')

WebUI.setText(findTestObject('Object Repository/Service_request/Page_IDHub Application/textarea_Search keywords_mat-input-2'), 
    'Requesting for new Keyboard')

WebUI.click(findTestObject('Object Repository/Service_request/Page_IDHub Application/div_Create new form_mat-select-arrow-wrappe_b62038'))

WebUI.click(findTestObject('Object Repository/Service_request/Page_IDHub Application/span_Account Creation'))

WebUI.click(findTestObject('Object Repository/Service_request/Page_IDHub Application/div_Custom form_mat-select-arrow-wrapper ng_c70da7'))

WebUI.click(findTestObject('Object Repository/Service_request/Page_IDHub Application/span_Access Manager Approval'))

WebUI.click(findTestObject('Object Repository/Service_request/Page_IDHub Application/div_Approver workflow_mat-slide-toggle-bar'))

WebUI.click(findTestObject('Object Repository/Service_request/Page_IDHub Application/button_Create'))

WebUI.setText(findTestObject('Object Repository/Service_request/Page_IDHub Application/input_Create_mat-input-3'), 'Submit new Service Request')

WebUI.click(findTestObject('Object Repository/Service_request/Page_IDHub Application/button_Submit'))

WebUI.click(findTestObject('Object Repository/Service_request/Page_IDHub Application/div_Tasks'))

WebUI.click(findTestObject('Object Repository/Service_request/Page_IDHub Application/span_Claim'))

WebUI.setText(findTestObject('Object Repository/Service_request/Page_IDHub Application/textarea_more_vert_mat-input-4'), 
    'Claim')

WebUI.click(findTestObject('Object Repository/Service_request/Page_IDHub Application/span_Claim_1'))

WebUI.click(findTestObject('Object Repository/Service_request/Page_IDHub Application/span_Approve'))

WebUI.setText(findTestObject('Object Repository/Service_request/Page_IDHub Application/textarea_more_vert_mat-input-5'), 
    'Approve')

WebUI.click(findTestObject('Object Repository/Service_request/Page_IDHub Application/span_Approve_1'))

WebUI.click(findTestObject('Object Repository/Service_request/Page_IDHub Application/div_Service Request'))

WebUI.click(findTestObject('Object Repository/Service_request/Page_IDHub Application/div_Keyboard  Created by YouLast modified  _963e35'))

WebUI.click(findTestObject('Object Repository/Service_request/Page_IDHub Application/mat-icon_arrow_drop_down'))

WebUI.click(findTestObject('Object Repository/Service_request/Page_IDHub Application/button_User App'))

WebUI.click(findTestObject('Object Repository/Service_request/Page_IDHub Application/span_Service Request'))

WebUI.setText(findTestObject('Object Repository/Service_request/Page_IDHub Application/input_Service Request_searchKeyword'), 
    'Keyboard')

WebUI.click(findTestObject('Object Repository/Service_request/Page_IDHub Application/span_Search'))

WebUI.click(findTestObject('Object Repository/Service_request/Page_IDHub Application/div_Logout'))

not_run: WebUI.navigateToUrl('https://dev7.iamsath.com/auth/realms/apple/protocol/openid-connect/auth?scope=openid&state=Ufwo_9OOvMfqrRnjFvJbC8fJE_FECPjlnh0HKbi0Y90.ySGLue86nDo.UserApplication&response_type=code&client_id=apple&redirect_uri=https%3A%2F%2Fdev7.iamsath.com%2Fauth%2Frealms%2FIDHub%2Fbroker%2Fapple%2Fendpoint&prompt=login&nonce=4Upup7lQnv8K_ALJuf2hfw')

not_run: WebUI.closeBrowser()

