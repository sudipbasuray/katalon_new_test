import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://dev7.iamsath.com/')

WebUI.click(findTestObject('Object Repository/Page_IDHub/button_Login (1)'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub/input_Login to your account_orgName (1)'), 'apple')

WebUI.sendKeys(findTestObject('Object Repository/Page_IDHub/input_Login to your account_orgName (1)'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/Page_Sign in to apple/input_Username or email_username (1)'), 'jerome')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Sign in to apple/input_Password_password (1)'), 'tNEMIZtZeaU=')

WebUI.sendKeys(findTestObject('Object Repository/Page_Sign in to apple/input_Password_password (1)'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/button_Manage Catalog (1)'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/mat-icon_arrow_drop_down (1)'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/button_User App (1)'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub Application/input_Service Request_searchKeyword (1)'), 'Automated App 1')

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/span_Search'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/img_Application_TargetSystem-Automated App _81e96e'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/div_1 (1)'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/div_Proceed (1)'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/div_Automated App 1 Applicationremove_circl_e9adee'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/button_Done (1)'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/button_Continue'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub Application/input_Jerome A.Smith_mat-input-3 (1)'), 'continue')

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/button_Submit (1)'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/button_Track Request (1)'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/div_Tasks1 (1)'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/span_Claim (1)'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub Application/textarea_more_vert_mat-input-4'), 'cl')

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/span_Claim_1 (1)'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/span_Complete (1)'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub Application/textarea_more_vert_mat-input-5 (1)'), 'cmp')

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/span_Complete_1'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/span_Jerome A.Smith (1)'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/div_Automated App 1  more_vert'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/img'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/div_Manage Catalog (1)'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub Application/input_Roles_searchKeyword (1)'), 'Automated App 1')

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/button_Search (1)'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/i_more_vert (1)'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/button_Retire Application (1)'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/button_Yes (1)'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/div_Logout (1)'))

WebUI.closeBrowser()

