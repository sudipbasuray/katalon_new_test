import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://dev7.iamsath.com/')

WebUI.click(findTestObject('Object Repository/Page_IDHub/button_Login'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub/input_Login to your account_orgName'), 'apple')

WebUI.click(findTestObject('Object Repository/Page_IDHub/button_Login_1'))

WebUI.setText(findTestObject('Object Repository/Page_Sign in to apple/input_Username or email_username'), 'jerome')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Sign in to apple/input_Password_password'), 'tNEMIZtZeaU=')

WebUI.click(findTestObject('Object Repository/Page_Sign in to apple/input_Forgot Password_login'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/button_Manage Catalog'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/mat-icon_arrow_drop_down'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/button_Add Application'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub Application/input_Add Application_mat-input-1'), 'Automated App 1')

WebUI.setText(findTestObject('Object Repository/Page_IDHub Application/textarea_Search Keywords_mat-input-2'), 'demo')

WebUI.setText(findTestObject('Object Repository/Page_IDHub Application/input_Add To Collection_mat-chip-list-input-4'), 
    'jerome')

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/span_Jerome A.Smith'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub Application/input_Business Owner_mat-chip-list-input-5'), 'jerome')

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/span_Jerome A.Smith'))

WebUI.click(findTestObject('Page_IDHub Application/span_'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/span_Disconnected'))

WebUI.click(findTestObject('Page_IDHub Application/span_ 2'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/span_Auto Approval'))

WebUI.click(findTestObject('Page_IDHub Application/span_3'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/span_Access Manager'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/div_Select Custom Form_mat-slide-toggle-bar'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/button_Next'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub Application/input_Add Application_mat-input-4'), 'user id')

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/div_IDE Field Name'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/span_Email'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/div_Data Type'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/span_String'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/div_Data Type_mat-slide-toggle-bar'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/div_Required_mat-slide-toggle-bar'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/div_Reconciliation key_mat-slide-toggle-bar'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/button_Add'))

WebUI.click(findTestObject('Page_IDHub Application/button_Next 2'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/button_Submit'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub Application/textarea_(0)_mat-input-8'), 'submit')

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/button_Submit'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/div_Tasks1'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/span_Claim'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub Application/textarea_more_vert_mat-input-10'), 'cl')

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/button_Claim'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/span_Approve'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub Application/textarea_more_vert_mat-input-11'), 'ap')

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/button_Approve'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/div_Manage Catalog'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub Application/input_Roles_searchKeyword'), 'Automated app 1')

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/button_Search'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/div_demo    demo3 Application edit more_vert'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Application/div_Logout'))

WebUI.closeBrowser()

