import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Basic/Basic Login to IDHub'), [('Username') : '', ('Password') : '', ('Organization') : ''], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/input_Roles_searchKeyword (1)'), 
    'Automation Engineer 1')

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/button_Search'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/mat-icon_edit'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/button_Next'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/button_Next'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/i_remove_circle_outline'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/img_Automation Engineer 1_add-icon disable-_8c6daa'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Submit (1)'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/textarea_remove_circle_outline_mat-input-3 (1)'), 
    'submit')

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Submit (1)'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/div_Tasks1 (1)'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Claim (1)'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/textarea_more_vert_mat-input-5 (1)'), 
    'cl')

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Claim_1 (1)'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Approve (1)'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/textarea_more_vert_mat-input-6 (1)'), 
    'ap')

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Approve_1 (1)'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/div_Manage Catalog (1)'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/input_Roles_searchKeyword_1'), 'automation engineer 1')

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Search (1)'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/div_Automation Engineer 1   Role edit more_vert (1)'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/div_Application  Entitlements'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/div_Manage Catalog (1)'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/i_more_vert'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Retire Role'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/button_Yes'))

WebUI.closeBrowser()

