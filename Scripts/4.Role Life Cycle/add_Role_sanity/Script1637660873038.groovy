import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Basic/Basic Login to IDHub'), [('Username') : '', ('Password') : '', ('Organization') : ''], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Create Role'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/input_Create Role_mat-input-1'), 
    GlobalVariable.addRole)

WebUI.setText(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/textarea_Search Keywords_mat-input-2'), 
    'demo')

WebUI.setText(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/input_Add To Collection_mat-chip-list-input-3'), 
    'jerome')

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Jerome A.Smith'))

WebUI.click(findTestObject('Page_IDHub Role/Page_IDHub Application/div_IDM_mat-select-arrow'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Auto Role Approval'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/div_Certifiable_mat-slide-toggle-bar'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Next'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/div_Create query to add users. This step is_0018d8'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Display Name'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/div_Select or type_mat-select-arrow'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_equals ()'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/div_Select or type_mat-select-arrow_1'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Jerome A.Smith'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Next'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/img_ADP_add-icon disable-select ng-star-inserted'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Submit'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/textarea_remove_circle_outline_mat-input-3'), 
    'submit')

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Submit'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/div_Tasks1'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Claim'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/textarea_more_vert_mat-input-5'), 
    'Cl')

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Claim_1'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Approve'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/textarea_more_vert_mat-input-6'), 
    'ap')

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Approve_1'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/div_Manage Catalog'))

WebUI.setText(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/input_Roles_searchKeyword'), 'Automation Engineer 1')

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/span_Search'))

WebUI.click(findTestObject('Object Repository/Page_IDHub Role/Page_IDHub Application/div_Automation Engineer 1   Role edit more_vert'))

WebUI.click(findTestObject('Page_IDHub Role/Page_IDHub Application/div_Manage Catalog'))

