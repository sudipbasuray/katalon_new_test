import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.nio.file.Path as Path
import java.nio.file.Paths as Paths

WebUI.callTestCase(findTestCase('Basic/Basic Login to IDHub'), [('Username') : '', ('Password') : '', ('Organization') : ''], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Reconciliation_Page/Page_IDHub Application/span_Applications'))

WebUI.setText(findTestObject('Object Repository/Reconciliation_Page/Page_IDHub Application/input_Roles_searchKeyword'), 
    'Discover')

WebUI.click(findTestObject('Object Repository/Reconciliation_Page/Page_IDHub Application/span_DiscoverOrg'))

WebUI.click(findTestObject('Object Repository/Reconciliation_Page/Page_IDHub Application/i_more_vert'))

WebUI.click(findTestObject('Object Repository/Reconciliation_Page/Page_IDHub Application/button_Reconciliation'))

WebUI.click(findTestObject('Object Repository/Reconciliation_Page/Page_IDHub Application/span_Upload'))

WebUI.uploadFile(findTestObject('Reconciliation_Page/Page_IDHub Application/span_Upload'), '/home/SuBa1020/Downloads/sample-template-DiscoverOrg.csv')

WebUI.click(findTestObject('Object Repository/Reconciliation_Page/Page_IDHub Application/span_Submit'))

WebUI.click(findTestObject('Object Repository/Reconciliation_Page/Page_IDHub Application/span_Yes'))

WebUI.click(findTestObject('Object Repository/Reconciliation_Page/Page_IDHub Application/span_Jerome A.Smith'))

WebUI.click(findTestObject('Object Repository/Reconciliation_Page/Page_IDHub Application/div_DiscoverOrg  more_vert  Nov 23, 2021 Ac_5a45e3'))

WebUI.click(findTestObject('Object Repository/Reconciliation_Page/Page_IDHub Application/div_No direct reports found_mat-drawer-back_72676f'))

WebUI.click(findTestObject('Object Repository/Reconciliation_Page/Page_IDHub Application/div_DiscoverOrg  more_vert'))

WebUI.click(findTestObject('Object Repository/Reconciliation_Page/Page_IDHub Application/div_Provisioned'))

WebUI.click(findTestObject('Object Repository/Reconciliation_Page/Page_IDHub Application/img'))

WebUI.click(findTestObject('Object Repository/Reconciliation_Page/Page_IDHub Application/div_Logout'))

WebUI.closeBrowser()

